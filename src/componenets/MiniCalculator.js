import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            result: 0
        };
        this.add = this.add.bind(this);
        this.subtract = this.subtract.bind(this);
        this.multiply = this.multiply.bind(this);
        this.divide = this.divide.bind(this);
    }

    render() {
        return (
            <section>
                <div>计算结果为:
                    <span className="result"> {this.state.result} </span>
                </div>
                <div className="operations">
                    <button onClick={this.add}>加1</button>
                    <button onClick={this.subtract}>减1</button>
                    <button onClick={this.multiply}>乘以2</button>
                    <button onClick={this.divide}>除以2</button>
                </div>
            </section>
        );
    }

    add() {
        let result = this.state.result;
        this.setState({result: ++result})
    }

    subtract() {
        let result = this.state.result;
        this.setState({result: --result})
    }

    multiply() {
        let result = this.state.result;
        this.setState({result: result * 2})
    }

    divide() {
        let result = this.state.result;
        this.setState({result: result / 2})
    }
}

export default MiniCalculator;

